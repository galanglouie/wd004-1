/**
 * Give at least three reasons why you should learn javascript.
 */

1. To make website interactive
2. It is use everywhere
3. Javascript can make some interesting projects

/**
 * In two sentences, state in your own words, what is your understanding about Javascript?
 */

1. javascript is use to do things that html and css cannot achieve
2. javascript is a way to have a logic on your website

/**
 * What is a variable? Give at least three examples.
 */

1. lets you assign a variable or holds data
Examples:
	1. var ten = "true"
	2. let weight = 25.2
	3. const name = "Louie"

/**
 * What are the data types we talked about today? Give at least two samples each.
 */

===> Here string, number, boolean
Examples:
	"Louie" // String
	99.9 // number
	true // boolean

/**
 * Write the correct data type of the following:
 */

1. "Number" - string
2. 158 - number
3. pi - number
4. false - boolean
5. 'true' - string
6. 'Not a String' string

/**
 * What are the arithmetic operators?
 */ +,-,/,*

/**
 * What is a modulo? 
 */ remainder

/**
 * Interpret the following code
 */ 

let number = 4;
number += 4;

Q: What is the value of number?

---- 8

let number = 4;
number -= 4;

Q: What is the value of number?

---- 4

let number = 4;
number *= 4;

Q: What is the value of number?

---- 16

let number = 4;
number /= 4;

Q: What is the value of number?

----- 4;
